EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Raspberry Pi Zero W 1.3 Extension"
Date "2022-01-12"
Rev "Rev1.3-0.1"
Comp "svddevelop"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Raspberry_Pi_Zero_W_(v1.3):Raspberry_Pi_Zero_W_(v1.3) IC1
U 1 1 61DC326F
P 2300 2050
F 0 "IC1" H 3750 2315 50  0000 C CNN
F 1 "Raspberry_Pi_Zero_W_(v1.3)" H 3750 2224 50  0000 C CNN
F 2 "Raspberry_Pi_Zero_W_(v1.3):RASPBERRYPIZEROWV13" H 5050 2150 50  0001 L CNN
F 3 "https://cdn.sparkfun.com/assets/learn_tutorials/6/7/6/PiZero_1.pdf" H 5050 2050 50  0001 L CNN
F 4 "Raspberry Pi Zero W (v1.3) Single-board Computers" H 5050 1950 50  0001 L CNN "Description"
F 5 "" H 5050 1850 50  0001 L CNN "Height"
F 6 "RASPBERRY-PI" H 5050 1750 50  0001 L CNN "Manufacturer_Name"
F 7 "Raspberry Pi Zero W (v1.3)" H 5050 1650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5050 1550 50  0001 L CNN "RS Part Number"
F 9 "" H 5050 1450 50  0001 L CNN "RS Price/Stock"
F 10 "Raspberry Pi Zero W (v1.3)" H 5050 1350 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5050 1250 50  0001 L CNN "Arrow Price/Stock"
	1    2300 2050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:AO3400A Q1
U 1 1 61DC1A5F
P 6950 3100
F 0 "Q1" H 7155 3146 50  0000 L CNN
F 1 "AO3400A" H 7155 3055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7150 3025 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3400A.pdf" H 6950 3100 50  0001 L CNN
	1    6950 3100
	1    0    0    -1  
$EndComp
Text GLabel 2100 2150 0    50   Input ~ 0
5V
Text GLabel 2100 2550 0    50   Input ~ 0
GND
Text GLabel 5400 3450 2    50   Input ~ 0
PIN26
Text GLabel 5400 3850 2    50   Input ~ 0
RUN1
Text GLabel 5400 3950 2    50   Input ~ 0
RUN2
Wire Wire Line
	2100 2150 2300 2150
Wire Wire Line
	2300 2550 2100 2550
Wire Wire Line
	5400 3850 5200 3850
Wire Wire Line
	5400 3950 5200 3950
Wire Wire Line
	5400 3450 5200 3450
$Comp
L Device:R_Small R1
U 1 1 61DC7208
P 6750 2900
F 0 "R1" H 6809 2946 50  0000 L CNN
F 1 "R_Small" H 6809 2855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6750 2900 50  0001 C CNN
F 3 "~" H 6750 2900 50  0001 C CNN
	1    6750 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 61DC7D9C
P 6750 3300
F 0 "R2" H 6809 3346 50  0000 L CNN
F 1 "R_Small" H 6809 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6750 3300 50  0001 C CNN
F 3 "~" H 6750 3300 50  0001 C CNN
	1    6750 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 61DC8B5A
P 7350 3400
F 0 "J1" H 7430 3392 50  0000 L CNN
F 1 "Cooler" H 7430 3301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7350 3400 50  0001 C CNN
F 3 "~" H 7350 3400 50  0001 C CNN
	1    7350 3400
	1    0    0    -1  
$EndComp
Text GLabel 7150 3650 3    50   Input ~ 0
GND
Text GLabel 7050 2700 1    50   Input ~ 0
5V
Text GLabel 6750 2700 1    50   Input ~ 0
PIN26
Wire Wire Line
	6750 2700 6750 2800
Wire Wire Line
	6750 3000 6750 3100
Connection ~ 6750 3100
Wire Wire Line
	6750 3100 6750 3200
Wire Wire Line
	7050 2900 7050 2700
Wire Wire Line
	7150 3400 7050 3400
Wire Wire Line
	7050 3400 7050 3300
Wire Wire Line
	7150 3500 7150 3650
Wire Wire Line
	6750 3400 6750 3500
Wire Wire Line
	6750 3500 7150 3500
Connection ~ 7150 3500
$Comp
L SSD1306-128x64_OLED:SSD1306 Brd1
U 1 1 61DD0A49
P 9700 5500
F 0 "Brd1" V 9746 5222 50  0000 R CNN
F 1 "SSD1306" V 9655 5222 50  0000 R CNN
F 2 "SSD1306:128x64OLED" H 9700 5750 50  0001 C CNN
F 3 "" H 9700 5750 50  0001 C CNN
	1    9700 5500
	0    -1   -1   0   
$EndComp
Text GLabel 9150 5350 0    50   Input ~ 0
SDA
Text GLabel 9150 5450 0    50   Input ~ 0
SCL
Text GLabel 9150 5550 0    50   Input ~ 0
3V3
Text GLabel 9150 5650 0    50   Input ~ 0
GND
Wire Wire Line
	9150 5350 9350 5350
Wire Wire Line
	9150 5450 9350 5450
Wire Wire Line
	9150 5550 9350 5550
Wire Wire Line
	9150 5650 9350 5650
Text Notes 1000 7000 0    50   ~ 0
https://www.raspberry-buy.de/I2C_OLED-Display_Raspberry_Pi_Python_SH1106_SSD1306.html
Text GLabel 2100 2250 0    50   Input ~ 0
SDA
Text GLabel 2100 3650 0    50   Input ~ 0
3V3
Text GLabel 2100 2850 0    50   Input ~ 0
GND
Text GLabel 5400 3650 2    50   Input ~ 0
GND
Text GLabel 5400 3150 2    50   Input ~ 0
GND
Text GLabel 5450 2750 2    50   Input ~ 0
GND
Text GLabel 5450 2250 2    50   Input ~ 0
GND
Text GLabel 2100 3950 0    50   Input ~ 0
GND
Text GLabel 2050 3350 0    50   Input ~ 0
GND
Text GLabel 2100 2450 0    50   Input ~ 0
SCL
Wire Wire Line
	2100 2250 2300 2250
Wire Wire Line
	2100 2450 2300 2450
Wire Wire Line
	2100 2850 2300 2850
Wire Wire Line
	2050 3350 2300 3350
Wire Wire Line
	2100 3650 2300 3650
Wire Wire Line
	2100 3950 2300 3950
Wire Wire Line
	5400 3150 5200 3150
Wire Wire Line
	5450 2750 5200 2750
Wire Wire Line
	5450 2250 5200 2250
Wire Wire Line
	5400 3650 5200 3650
$Comp
L Switch:SW_SPST SW1
U 1 1 61DDB7B1
P 2450 5200
F 0 "SW1" H 2450 5435 50  0000 C CNN
F 1 "SW_SPST" H 2450 5344 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 2450 5200 50  0001 C CNN
F 3 "~" H 2450 5200 50  0001 C CNN
	1    2450 5200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 61DDC23B
P 2450 5400
F 0 "SW2" H 2450 5635 50  0000 C CNN
F 1 "SW_SPST" H 2450 5544 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 2450 5400 50  0001 C CNN
F 3 "~" H 2450 5400 50  0001 C CNN
	1    2450 5400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 61DDC72A
P 2450 5600
F 0 "SW3" H 2450 5835 50  0000 C CNN
F 1 "SW_SPST" H 2450 5744 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 2450 5600 50  0001 C CNN
F 3 "~" H 2450 5600 50  0001 C CNN
	1    2450 5600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW4
U 1 1 61DDCC00
P 2450 5750
F 0 "SW4" H 2450 5985 50  0000 C CNN
F 1 "SW_SPST" H 2450 5894 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 2450 5750 50  0001 C CNN
F 3 "~" H 2450 5750 50  0001 C CNN
	1    2450 5750
	1    0    0    -1  
$EndComp
Text GLabel 2050 5200 0    50   Input ~ 0
PIN25
Text GLabel 2050 5400 0    50   Input ~ 0
PIN24
Text GLabel 2050 5600 0    50   Input ~ 0
PIN23
Text GLabel 2050 5750 0    50   Input ~ 0
PIN22
Text GLabel 2050 5900 0    50   Input ~ 0
GND
Wire Wire Line
	2050 5900 2850 5900
Wire Wire Line
	2850 5200 2650 5200
Wire Wire Line
	2650 5400 2850 5400
Connection ~ 2850 5400
Wire Wire Line
	2850 5400 2850 5200
Wire Wire Line
	2650 5600 2850 5600
Connection ~ 2850 5600
Wire Wire Line
	2850 5600 2850 5400
Wire Wire Line
	2650 5750 2850 5750
Wire Wire Line
	2850 5600 2850 5750
Connection ~ 2850 5750
Wire Wire Line
	2850 5750 2850 5900
Wire Wire Line
	2250 5200 2050 5200
Wire Wire Line
	2250 5400 2050 5400
Wire Wire Line
	2250 5600 2050 5600
Wire Wire Line
	2250 5750 2050 5750
Text GLabel 2100 4150 0    50   Input ~ 0
PIN25
Text GLabel 2100 3750 0    50   Input ~ 0
PIN24
Text GLabel 2100 3550 0    50   Input ~ 0
PIN23
Text GLabel 2100 3450 0    50   Input ~ 0
PIN22
Wire Wire Line
	2100 3450 2300 3450
Wire Wire Line
	2100 3550 2300 3550
Wire Wire Line
	2100 3750 2300 3750
Wire Wire Line
	2100 4150 2300 4150
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 61DF7C5A
P 7950 5200
F 0 "J2" H 8030 5192 50  0000 L CNN
F 1 "UART" H 8030 5101 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7950 5200 50  0001 C CNN
F 3 "~" H 7950 5200 50  0001 C CNN
	1    7950 5200
	1    0    0    -1  
$EndComp
Text GLabel 7450 5100 0    50   Input ~ 0
5V
Text GLabel 7450 5400 0    50   Input ~ 0
GND
$Comp
L Transistor_FET:AO3400A Q2
U 1 1 61DF876F
P 6850 5250
F 0 "Q2" V 7192 5250 50  0000 C CNN
F 1 "AO3400A" V 7200 5000 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7050 5175 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3400A.pdf" H 6850 5250 50  0001 L CNN
	1    6850 5250
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_FET:AO3400A Q3
U 1 1 61DFE64D
P 6850 5950
F 0 "Q3" V 7192 5950 50  0000 C CNN
F 1 "AO3400A" V 6750 5700 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7050 5875 50  0001 L CIN
F 3 "http://www.aosmd.com/pdfs/datasheet/AO3400A.pdf" H 6850 5950 50  0001 L CNN
	1    6850 5950
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R5
U 1 1 61E00DB0
P 6600 5750
F 0 "R5" H 6659 5796 50  0000 L CNN
F 1 "R_Small" H 6150 5750 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6600 5750 50  0001 C CNN
F 3 "~" H 6600 5750 50  0001 C CNN
	1    6600 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 61E014A4
P 7100 5750
F 0 "R6" H 7159 5796 50  0000 L CNN
F 1 "R_Small" H 7159 5705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7100 5750 50  0001 C CNN
F 3 "~" H 7100 5750 50  0001 C CNN
	1    7100 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 61E01BC3
P 6600 5050
F 0 "R3" H 6450 5050 50  0000 L CNN
F 1 "R_Small" H 6100 5050 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 6600 5050 50  0001 C CNN
F 3 "~" H 6600 5050 50  0001 C CNN
	1    6600 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 61E02387
P 7100 5050
F 0 "R4" H 7159 5096 50  0000 L CNN
F 1 "R_Small" H 7159 5005 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" H 7100 5050 50  0001 C CNN
F 3 "~" H 7100 5050 50  0001 C CNN
	1    7100 5050
	1    0    0    -1  
$EndComp
Text GLabel 6500 5150 0    50   Input ~ 0
RX
Text GLabel 6500 5850 0    50   Input ~ 0
TX
Text GLabel 2100 2750 0    50   Input ~ 0
TX
Text GLabel 2100 2950 0    50   Input ~ 0
RX
Wire Wire Line
	2100 2750 2300 2750
Wire Wire Line
	2100 2950 2300 2950
Wire Wire Line
	6600 5150 6650 5150
Wire Wire Line
	6500 5150 6600 5150
Connection ~ 6600 5150
Wire Wire Line
	6850 5450 6700 5450
Wire Wire Line
	6700 5450 6700 5300
Wire Wire Line
	6700 5300 6600 5300
Wire Wire Line
	6600 5300 6600 5150
Wire Wire Line
	7050 5150 7100 5150
Wire Wire Line
	7450 5100 7600 5100
Wire Wire Line
	7450 5400 7750 5400
Wire Wire Line
	7200 5300 7750 5300
Wire Wire Line
	7100 5200 7100 5150
Connection ~ 7100 5150
Wire Wire Line
	7750 5200 7100 5200
Text GLabel 6500 4950 0    50   Input ~ 0
3V3
Text GLabel 6500 5650 0    50   Input ~ 0
3V3
Wire Wire Line
	6500 4950 6600 4950
Wire Wire Line
	7100 4950 7600 4950
Wire Wire Line
	7600 4950 7600 5100
Connection ~ 7600 5100
Wire Wire Line
	7600 5100 7750 5100
Text GLabel 7100 5600 1    50   Input ~ 0
5V
Wire Wire Line
	6500 5650 6600 5650
Wire Wire Line
	6500 5850 6600 5850
Wire Wire Line
	6600 5850 6650 5850
Connection ~ 6600 5850
Wire Wire Line
	6850 6150 6700 6150
Wire Wire Line
	6700 6150 6700 6000
Wire Wire Line
	6700 6000 6600 6000
Wire Wire Line
	6600 6000 6600 5850
Wire Wire Line
	7050 5850 7100 5850
Wire Wire Line
	7200 5850 7100 5850
Wire Wire Line
	7200 5300 7200 5850
Connection ~ 7100 5850
Wire Wire Line
	7100 5650 7100 5600
Wire Notes Line
	3300 4800 3300 6250
Wire Notes Line
	3300 6250 1350 6250
Wire Notes Line
	1350 6250 1350 4800
Wire Notes Line
	1350 4800 3300 4800
Text Notes 1400 4900 0    50   ~ 0
functional keypad
Wire Notes Line
	3450 4800 5700 4800
Wire Notes Line
	5700 4800 5700 6250
Wire Notes Line
	5700 6250 3450 6250
Wire Notes Line
	3450 6250 3450 4800
Text Notes 3500 4900 0    50   ~ 0
Restart button
Wire Wire Line
	4950 5700 4950 5550
Wire Wire Line
	4400 5700 4950 5700
Wire Wire Line
	4400 5550 4550 5550
Text GLabel 4400 5700 0    50   Input ~ 0
RUN2
Text GLabel 4400 5550 0    50   Input ~ 0
RUN1
$Comp
L Switch:SW_SPST SW5
U 1 1 61DDAD6D
P 4750 5550
F 0 "SW5" H 4750 5785 50  0000 C CNN
F 1 "Restart" H 4750 5694 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_Vertical_Wuerth_434133025816" H 4750 5550 50  0001 C CNN
F 3 "~" H 4750 5550 50  0001 C CNN
	1    4750 5550
	1    0    0    -1  
$EndComp
Wire Notes Line
	5850 4800 8350 4800
Wire Notes Line
	8350 4800 8350 6250
Wire Notes Line
	8350 6250 5850 6250
Wire Notes Line
	5850 6250 5850 4800
Text Notes 7800 6200 0    50   ~ 0
UART Adapter
Wire Notes Line
	8500 4800 10600 4800
Wire Notes Line
	10600 4800 10600 6250
Wire Notes Line
	10600 6250 8500 6250
Wire Notes Line
	8500 6250 8500 4800
Text Notes 8550 4900 0    50   ~ 0
Mini display
Wire Notes Line
	6400 1700 7900 1700
Wire Notes Line
	7900 1700 7900 4300
Wire Notes Line
	7900 4300 6400 4300
Wire Notes Line
	6400 4300 6400 1700
Text Notes 6450 1800 0    50   ~ 0
Cooler
$EndSCHEMATC
